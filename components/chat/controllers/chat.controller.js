app.controller('ChatCtrl', ['$scope','$timeout', function ($scope, $timeout){
	$scope.model 	= {};
	$scope.uiAction = {};
	$scope.uiAction.msg 		= "";
	$scope.uiAction.botIsTyping = false;

	$scope.model.chats = [
		{
			"tipo":"BOT",
			"classes":"chat--reply text-left",
			"msg":"Hola, como estás?",
			"name":"Asistente virtual"
		},
		{
			"tipo":"USER",
			"classes":"chat--user text-right",
			"msg":"Hola!",
			"name":"Alex"
		},
		{
			"tipo":"USER",
			"classes":"chat--user text-right",
			"msg":"Hay alguien?",
			"name":"Alex"
		},
		{
			"tipo":"BOT",
			"classes":"chat--reply text-left",
			"msg":"No",
			"name":"Asistente virtual"
		}
	];

	$scope.pushMsg 	= function (){
		var chat = {
			"tipo":"USER",
			"classes":"chat--user text-right",
			"msg": $scope.uiAction.msg,
			"name":"Alex"
		}
		$scope.model.chats.push(chat);
		$scope.uiAction.msg = "";

		// REPLY BOT
		$scope.replyBot();
	};

	$scope.replyBot 	= function (){
		$scope.uiAction.botIsTyping = true;
		
		$timeout( function (){
			var botChat = {
				"tipo":"BOT",
				"classes":"chat--reply text-left",
				"msg":"No hay nadie. Vuelva mañana.",
				"name":"Asistente virtual"
			}

			$scope.model.chats.push(botChat);
			$scope.uiAction.botIsTyping = false;

			console.log("botChat: ", $scope.model.chats);
		}, 2000);
	};

}]);